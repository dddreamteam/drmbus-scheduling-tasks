package com.drmbus.schedulingtasks.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:db.properties")
public class DbConfiguration {


	@Value("${db.url}")
	private String dbURL;

	@Value("${db.login}")
	private String dbLogin;

	@Value("${db.password}")
	private String dbPassword;


}
