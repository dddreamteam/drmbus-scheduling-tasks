package com.drmbus.schedulingtasks.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


@Component
@PropertySource("classpath:db.properties")
public class VoyageGenTask {
    private static final Logger log = LoggerFactory.getLogger(VoyageGenTask.class);

    @Value("${db.url}")
    private String dbURL;

    @Value("${db.login}")
    private String dbLogin;

    @Value("${db.password}")
    private String dbPassword;

    //@Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Kiev")
    @Scheduled(cron = "0/10 0 0 * * ?", zone = "Europe/Kiev")
    public void reportCurrentTime() {
        voyageGenProcedure();
    }

    public void voyageGenProcedure() {
        try (Connection connection =
                     DriverManager.getConnection(
                             dbURL,
                             dbLogin,
                             dbPassword)) {
            CallableStatement proc =
                    connection.prepareCall("{ call voyage_auto_gen_new() }");

            proc.execute();

            log.info("Voyages updated");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
